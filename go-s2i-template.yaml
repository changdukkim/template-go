kind: Template
apiVersion: v1
metadata:
  name: golang-s2i-build
  annotations:
    openshift.io/display-name: Golang-s2i-build
    description: golang-s2i-build
    iconClass: icon-go-gopher
    openshift.io/provider-display-name: Golang-s2i-build
labels:
  template: golang-s2i-build
parameters:
- description: The name for the application.
  displayName: Application Name
  name: APPLICATION_NAME
  required: true
  value: golang-example
- description: Git source URI for application
  displayName: Git Repository URL
  name: SOURCE_REPOSITORY_URL
  required: true
  value: https://github.com/sclorg/golang-ex.git
- description: Git branch/tag reference
  displayName: Git Reference
  name: SOURCE_REPOSITORY_REF
  value: "master"
- description: Path within Git project to build; empty for root project directory.
  displayName: Context Directory
  name: CONTEXT_DIR
  value: /
- description: GitHub trigger secret
  displayName: Github Webhook Secret
  from: '[a-zA-Z0-9]{8}'
  generate: expression
  name: GITHUB_WEBHOOK_SECRET
  required: true
- description: Generic build trigger secret
  displayName: Generic Webhook Secret
  from: '[a-zA-Z0-9]{8}'
  generate: expression
  name: GENERIC_WEBHOOK_SECRET
  required: true
- description: The exposed hostname that will route to the nginx service, if left
    blank a value will be defaulted.
  displayName: Application Hostname
  name: APPLICATION_DOMAIN
objects:
- apiVersion: v1
  kind: Service
  metadata:
    annotations:
      description: Exposes and load balances the application pods
    name: ${APPLICATION_NAME}
  spec:
    ports:
    - name: web
      port: 8080
      targetPort: 8080
    selector:
      deploymentConfig: ${APPLICATION_NAME}
- apiVersion: v1
  kind: Route
  metadata:
    annotations:
      template.openshift.io/expose-uri: http://{.spec.host}{.spec.path}
    name: ${APPLICATION_NAME}
  spec:
    host: ${APPLICATION_DOMAIN}
    to:
      kind: Service
      name: ${NAME}
- apiVersion: v1
  kind: ImageStream
  metadata:
    labels:
      application: ${APPLICATION_NAME}
    name: ${APPLICATION_NAME}
- apiVersion: v1
  kind: BuildConfig
  metadata:
    labels:
      application: ${APPLICATION_NAME}
    name: ${APPLICATION_NAME}
  spec:
    output:
      to:
        kind: ImageStreamTag
        name: ${APPLICATION_NAME}:latest
    source:
      contextDir: ${CONTEXT_DIR}
      git:
        ref: ${SOURCE_REPOSITORY_REF}
        uri: ${SOURCE_REPOSITORY_URL}
      type: Git
    strategy:
      sourceStrategy:
        forcePull: true
        from:
          kind: ImageStreamTag
          name: golang:1.10
          namespace: openshift
      type: Source
    triggers:
    - github:
        secret: ${GITHUB_WEBHOOK_SECRET}
      type: GitHub
    - generic:
        secret: ${GENERIC_WEBHOOK_SECRET}
      type: Generic
    - imageChange: {}
      type: ImageChange
    - type: ConfigChange
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      application: ${APPLICATION_NAME}
    name: ${APPLICATION_NAME}
  spec:
    replicas: 1
    selector:
      deploymentConfig: ${APPLICATION_NAME}
    strategy:
      type: Rolling
    template:
      metadata:
        labels:
          application: ${APPLICATION_NAME}
          deploymentConfig: ${APPLICATION_NAME}
        name: ${APPLICATION_NAME}
      spec:
        containers:
        - env:
          image: ${APPLICATION_NAME}
          imagePullPolicy: Always
          name: ${APPLICATION_NAME}
          ports:
          - containerPort: 8080
            name: http
            protocol: TCP
            exec:
              command:
              - /bin/bash
              - -c
              - exec /opt/app-root/gobinary
    triggers:
    - imageChangeParams:
        automatic: true
        containerNames:
        - ${APPLICATION_NAME}
        from:
          kind: ImageStreamTag
          name: ${APPLICATION_NAME}:latest
      type: ImageChange
    - type: ConfigChange

